
FROM openjdk:8-jdk-alpine
MAINTAINER rindai.webster
COPY target/swagger-doc.jar swagger-doc.jar
RUN apk update && apk add bash
ENTRYPOINT ["java","-jar","/swagger-doc.jar"]
